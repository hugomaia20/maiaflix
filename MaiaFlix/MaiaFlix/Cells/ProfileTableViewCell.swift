//
//  ProfileTableViewCell.swift
//  MaiaFlix
//
//  Created by Hugo  Maia on 08/03/2022.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var Username: UILabel!
    @IBOutlet weak var buttonFavourites: UIButton!
    @IBOutlet weak var buttonEditProfile: UIButton!
    @IBOutlet weak var buttonLogOut: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        class ProfileTableViewCell: UITableViewCell {
        }
        
    }
    
   
    
}

@IBDesignable extension UIButton {

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}
