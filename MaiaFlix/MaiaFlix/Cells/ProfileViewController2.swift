//
//  ProfileViewController2.swift
//  MaiaFlix
//
//  Created by Hugo  Maia on 08/03/2022.
//

import UIKit

class ProfileViewController2: UIViewController {

    @IBOutlet weak var profileFeedTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //regista o tipo de celula na minha tabela
        profileFeedTable.register(UINib(nibName: "ProfileTableViewCell",
                       bundle: nil),
                    forCellReuseIdentifier: "ProfileTableViewCell")
        
        profileFeedTable.delegate  = self
        profileFeedTable.dataSource = self

        // Do any additional setup after loading the view.
    }


}

//numero de secções da tabela
extension ProfileViewController2: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    //numero de linhas da tabela
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    //atribuição de valores aos dados
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
        cell.imageProfile.image = UIImage(named: "person-icon")
        cell.Username.text = "Hugo"
        cell.buttonFavourites.setTitle("Favourites", for: .normal)
        cell.buttonEditProfile.setTitle("Edit Profile", for: .normal)
        cell.buttonLogOut.setTitle("Log Out", for: .normal)
        
        return cell
    }
    
    
    //altura da tabela
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 700
    }
    
}
