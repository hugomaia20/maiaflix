//
//  HeaderImageUIView.swift
//  MaiaFlix
//
//  Created by Hugo  Maia on 08/03/2022.
//

import UIKit

class HeaderImageUIView: UIView {

    private let headerImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = UIImage(named:"SpiderMan")
        return imageView
    }()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        addSubview(headerImageView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        headerImageView.frame = bounds
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }
}
