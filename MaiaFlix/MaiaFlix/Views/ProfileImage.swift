//
//  ProfileImage.swift
//  MaiaFlix
//
//  Created by Hugo  Maia on 08/03/2022.
//

import UIKit

class ProfileImage: UIView {

    private let profileImageView: UIImageView = {
        let profileImage = UIImageView()
        profileImage.contentMode = .scaleAspectFill
        profileImage.clipsToBounds = true
        profileImage.image = UIImage(named:"person.circle")
        return profileImage
    }()
    
    override init(frame: CGRect){
        super.init(frame: frame)
        addSubview(profileImageView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.frame = bounds
    }
    
    required init?(coder: NSCoder) {
        fatalError()
    }

}
