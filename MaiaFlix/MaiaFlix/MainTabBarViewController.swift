//
//  ViewController.swift
//  MaiaFlix
//
//  Created by Hugo  Maia on 07/03/2022.
//
import AVFoundation
import UIKit

class MainTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //color background
        view.backgroundColor = .systemBackground
        
        //variables of tabbar
        let vc1 = UINavigationController(rootViewController: HomeViewController())
        let vc2 = UINavigationController(rootViewController: SearchViewController())
        let vc3 = UINavigationController(rootViewController: ProfileViewController2(nibName: "ProfileViewController2", bundle: nil))
        
        //atribuiton of icons
        vc1.tabBarItem.image = UIImage(systemName:"house.fill")
        vc2.tabBarItem.image = UIImage(systemName:"magnifyingglass")
        vc3.tabBarItem.image = UIImage(systemName:"person.fill")
        
        //variables titles
        vc1.title = "Home"
        vc2.title = "Search"
        vc3.title = "Profile"
        
        //tabBar
        setViewControllers([vc1,vc2,vc3], animated: true)

    }

    
}

