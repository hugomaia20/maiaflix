//
//  ProfileViewController.swift
//  MaiaFlix
//
//  Created by Hugo  Maia on 07/03/2022.
//

import UIKit

class ProfileViewController: UIViewController {
    

//declaração da tabela
    private let profileFeedTable: UITableView =   {
        let table = UITableView(frame: .zero,style: .grouped)
        table.register(UINib(nibName: "ProfileTableViewCell",
                       bundle: nil),
                    forCellReuseIdentifier: "ProfileTableViewCell")
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(profileFeedTable)
        
        
        profileFeedTable.delegate  = self
        profileFeedTable.dataSource = self
        
        view.backgroundColor = .systemBackground
        
        let profileImage = ProfileImage(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: 450))
        profileFeedTable.tableHeaderView = profileImage
        
        
        
        
        
        
    }
    
}
extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    //numero de secções da tabela
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    //numero de linhas da tabela
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    //atribuição de valores aos dados
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
         let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
        cell.imageProfile.image = UIImage(named: "person-icon")
        cell.Username.text = "Hugo"
        cell.buttonFavourites.setTitle("Favourites", for: .normal)
        cell.buttonEditProfile.setTitle("Edit Profile", for: .normal)
        cell.buttonLogOut.setTitle("Log Out", for: .normal)
        
        
        return cell
    }
    //altura da tabela
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
}
