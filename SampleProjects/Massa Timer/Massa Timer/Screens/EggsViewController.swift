//
//  EggsViewController.swift
//  Massa Timer
//
//  Created by Hugo  Maia on 23/03/2022.
//

import UIKit
import AVFoundation

class EggsViewController: UIViewController {
    
    
    @IBOutlet weak var doneLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    var eggsTimes = ["Soft": 300,"Hard": 720]
    
    var totalTime = 0
    
    var secondsPassed = 0
    
    var timer = Timer()
    
    var player: AVAudioPlayer!
    

    @IBAction func eggButon(_ sender: UIButton) {
    
        if let buttonTitle = sender.titleLabel?.text,
           let unwrapedTotalTime = eggsTimes[buttonTitle]{
            
            timer.invalidate()
            
            totalTime = unwrapedTotalTime
            
            secondsPassed = 0
            
            doneLabel.text = buttonTitle
            
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        }
        
    }
    
    @objc func update(){
        if secondsPassed < totalTime{
            secondsPassed += 1
            
            progressBar.progress = Float(secondsPassed) / Float(totalTime)
        }else{
            timer.invalidate()
            doneLabel.text = "Done"
            
            let url = Bundle.main.url(forResource: "alarm_sound", withExtension: "mp3")
            
            do{
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
                    try AVAudioSession.sharedInstance().setActive(true)
                    player = try! AVAudioPlayer(contentsOf: url!)

                }

                catch{

                    print(error)
                }
            player.play()
        }
    }
    
}
