//
//  MainViewController.swift
//  Massa Timer
//
//  Created by Hugo  Maia on 23/03/2022.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }


    
    @IBAction func riceButton(_ sender: Any) {
        let riceVC = RiceViewController(nibName: "RiceViewController", bundle: nil)

        present(riceVC, animated: true)
    }
    
    @IBAction func pastaButton(_ sender: Any) {
        let mainSB = UIStoryboard(name: "Main", bundle: nil)
        
        let otherVC = mainSB.instantiateViewController(withIdentifier: "Pasta")
        
        present(otherVC, animated: true)
    }
    
    @IBAction func eggsButton(_ sender: Any) {
        let eggVC = EggsViewController(nibName: "EggsViewController", bundle: nil)
        present(eggVC, animated: true)
    }
    
    @IBAction func potatosButton(_ sender: Any) {
        let potatoVC = PotatoViewController(nibName: "PotatoViewController", bundle: nil)
        present(potatoVC, animated: true)
    }
}
