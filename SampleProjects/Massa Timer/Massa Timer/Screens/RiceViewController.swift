//
//  RiceViewController.swift
//  Massa Timer
//
//  Created by Hugo  Maia on 23/03/2022.
//

import UIKit
import AVFoundation

class RiceViewController: UIViewController {

    
    @IBOutlet weak var doneLabel: UILabel!
    
    @IBOutlet weak var progressaBar: UIProgressView!
    
    var riceTimes = ["Soft": 720 , "Hard": 600]
    
    var timer = Timer()
    
    var totalTime = 0
    
    var secondsPassed = 0
    
    var player: AVAudioPlayer!




    @IBAction func buttonTime(_ sender: UIButton) {
        
        if let buttonTitle = sender.titleLabel?.text,
           let unwrapedunwrapedTotalTime = riceTimes[buttonTitle]{
            timer.invalidate()
            
            totalTime = unwrapedunwrapedTotalTime
            
            secondsPassed = 0
            
            doneLabel.text = buttonTitle
            
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(update), userInfo: nil, repeats: true)
            
            
        }
        
    }
    
    @objc func update(){
        
        if secondsPassed < totalTime {
            secondsPassed += 1
            progressaBar.progress = Float(secondsPassed) / Float(totalTime)
        }else{
            timer.invalidate()
            doneLabel.text = "Done"
            let url = Bundle.main.url(forResource: "alarm_sound", withExtension: "mp3")
            
            do{
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
                    try AVAudioSession.sharedInstance().setActive(true)
                    player = try! AVAudioPlayer(contentsOf: url!)

                }

                catch{

                    print(error)
                }
            player.play()
        }
    }
}
