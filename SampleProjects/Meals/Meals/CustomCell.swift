//
//  CustomCell.swift
//  Meals
//
//  Created by Hugo  Maia on 31/03/2022.
//

import UIKit

class CustomCell: UITableViewCell {

    @IBOutlet weak var mealsImage: UIImageView!
    @IBOutlet weak var mealsLbl1: UILabel!
    @IBOutlet weak var mealsLbl2: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
