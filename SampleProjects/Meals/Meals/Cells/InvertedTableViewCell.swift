//
//  InvertedTableViewCell.swift
//  Meals
//
//  Created by Hugo  Maia on 31/03/2022.
//

import UIKit

class InvertedTableViewCell: UITableViewCell {

    @IBOutlet weak var invertedImage: UIImageView!
    @IBOutlet weak var InvertedTitle: UILabel!
    @IBOutlet weak var invertedDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
