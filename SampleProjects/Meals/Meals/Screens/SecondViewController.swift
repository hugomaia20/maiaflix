//
//  SecondViewController.swift
//  Meals
//
//  Created by Hugo  Maia on 31/03/2022.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var lbl2Second: UILabel!
    @IBOutlet weak var lbl1Second: UILabel!
    @IBOutlet weak var imgSecond: UIImageView!
    
    var image: UIImage?
    var titleReceibed: String?
    var descriptioReceibed: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        
        lbl2Second.text = descriptioReceibed
        lbl1Second.text = titleReceibed
        imgSecond.image = image
    }

}
