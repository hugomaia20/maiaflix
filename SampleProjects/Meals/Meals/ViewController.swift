//
//  ViewController.swift
//  Meals
//
//  Created by Hugo  Maia on 31/03/2022.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var mealsView: UITableView!
    
    let image = ["batatas","egg","esparguete","rice"]
    
    let titles = ["Potatos","Eggs","Pasta","Rice"]
    
    let prepTime = ["20","30","40","50"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        mealsView.delegate = self
        mealsView.dataSource = self
        self.registerTableViewCells()
    }
    
    private func registerTableViewCells() {
        //Regista o tipo de celulas
        let mealCell = UINib(nibName: "Test",
                             bundle: nil)
        self.mealsView.register(mealCell,
                                forCellReuseIdentifier: "Test")
        let mealCell2 = UINib(nibName: "InvertedTableViewCell",
                             bundle: nil)
        self.mealsView.register(mealCell2,
                                forCellReuseIdentifier: "Inverted")
    }
}

extension ViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return image.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = SecondViewController(nibName: "SecondViewController", bundle: nil)
        if let vcimg = UIImage(named: image[indexPath.row]){
            vc.image = vcimg
            vc.titleReceibed = titles[indexPath.row]
            vc.descriptioReceibed = "Prep time: \(prepTime[indexPath.row]) min"
        }
        present(vc, animated: true)
        
        tableView.cellForRow(at: indexPath)?.setSelected(false, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.item % 2 == 0{
        let cell = mealsView.dequeueReusableCell(withIdentifier: "Test") as! Test
            cell.mealImage.image = UIImage(named: image[indexPath.row])
            cell.mealLbl1.text = titles[indexPath.row]
            cell.mealLbl2.text = "Prep time: \(prepTime[indexPath.row]) min"
            
            return cell
        }else{
            let cell2 = mealsView.dequeueReusableCell(withIdentifier: "Inverted") as! InvertedTableViewCell
            cell2.invertedImage.image = UIImage(named: image[indexPath.row])
            cell2.InvertedTitle.text = titles[indexPath.row]
            cell2.invertedDescription.text = "Prep time: \(prepTime[indexPath.row]) min"
            
            return cell2
        }
        
        
        
        
    }
    
}
